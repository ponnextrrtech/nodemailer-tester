const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const handlebars = require('handlebars');
const fs = require('fs');

const transporter = nodemailer.createTransport(smtpTransport({
  service: 'gmail',
  host: 'smtp.gmail.com',
  auth: {
    user: 'tech@hardhatasia.com',
    pass: 'hhasia@123'
	}
}));

function emailBuilder() {
  const source = fs.readFileSync('./hardhat.html', 'utf-8').toString();
  const template = handlebars.compile(source);
  const replacements = {};
  const htmlToSend = template(replacements);
	return {
		from: {
			name: 'HardHat Asia',
			address: 'no-reply@support.hardhatasia.com'
		},
		to: 'ponnex@trrtech.ph',
		subject: 'Subject',
		html: htmlToSend,
	}
}

transporter.sendMail(emailBuilder());