# HardHat Asia Mail tester

## Build Setup

```bash
# install dependencies to validate that there are no mismatched versions
$ npm ci

# install dependencies
$ npm install

# Send to test HTML email
$ node index.js
```
### Important reminder
Don't forget to change the `to` to the emailBuilder function where the email will be sent on `index.js`